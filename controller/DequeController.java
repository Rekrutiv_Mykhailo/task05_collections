package com.epam.controller;

import com.epam.model.deque.MyDeque;

public class DequeController<E> {
    private MyDeque<E> myDequey;

    public DequeController(MyDeque<E> myDeque) {
        this.myDequey = myDeque;
    }

    public DequeController() {
    }

    public void removeFirstElement() {
        this.myDequey.pollFirst();
    }

    public void removeLastElement() {
        this.myDequey.pollLast();
    }

    public void addFirstElement(E e) {
        this.myDequey.offerFirst(e);
    }

    public void addLastElement(E e) {
        this.myDequey.offerLast(e);
    }

    public String loadAllDeque() {
        return myDequey.toString();
    }
}
