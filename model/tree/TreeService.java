package com.epam.model.tree;

import java.util.TreeMap;

public interface TreeService<K,V> {
    Node find(Node node, K key);
    Node insert(Node node,K key, V value);
    void delete(Node node,K key);

}
