package com.epam.model.tree;

public class Tree<K, V> implements TreeService<K, V> {

    private Node root;

    public Tree() {
    }

    public Tree(Node root) {
        this.root = root;
    }

    @Override
    public Node find(Node node, K key) {
        Node current = root;
        while (current.getKey() != key) {
            if (node.equals(current))
                current = current.getLeftChild();
            else if (key.toString().compareTo(node.getKey().toString()) > 0)
                current = current.getRightChild();
            if (current == null)
                return null;
        }
        return current;
    }

    @Override
    public Node insert(Node node, K key, V value) {
        if (node == null) {
            node = new Node(key, value);
        } else {
            if (key.toString().compareTo(node.getKey().toString()) < 0) {
                node.setLeftChild(insert(node.getLeftChild(), key, value));
            } else if (key.toString().compareTo(node.getKey().toString()) > 0) {
                node.setRightChild(insert(node.getRightChild(), key, value));
            } else {
                node = insert(node, key, value);
            }
        }
        return node;
    }

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }

    @Override
    public void delete(Node node, K key) {
        {
            Node current = root;
            while (current.getKey() != key) {
                if (key.toString().compareTo(current.getKey().toString()) < 0) {
                    current = current.getLeftChild();
                }
                if (key.toString().compareTo(current.getKey().toString()) > 0) {
                    current = current.getRightChild();
                }
                if (current.getKey().equals(key)) {
                    find(node, (K) current.getKey()).setDeleted(true);
                }
            }
        }
    }
}