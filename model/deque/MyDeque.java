package com.epam.model.deque;

import com.epam.model.deque.DequeUtility;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class MyDeque<E> implements DequeUtility<E> {
    private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;
    private static final int FIRST_ELEMENT_INDEX = 0;
    private static final int MASSIVE_SIZE = 16;
    private Object[] elements;
    private int tail;
    private int head;

    public MyDeque() {
        this.elements = new Object[MASSIVE_SIZE];
        tail = 0;
    }

    public MyDeque(Object[] elements) {
        this.elements = elements;
        tail = elements.length;
    }

    public static int getMaxArraySize() {
        return MAX_ARRAY_SIZE;
    }

    @Override
    public void addFirst(E e) {
        System.out.println(elements.length + "  s " + tail);
        if (e == null) {
            throw new NullPointerException();
        } else if (tail >= elements.length && tail != FIRST_ELEMENT_INDEX) {
            this.increaseArraySize();
            this.addFirst(e);
        } else if (tail < elements.length) {
            this.addToBeginng(e);
            tail++;
        } else if (elements.length == 0) {
            elements = new Object[MASSIVE_SIZE];
            addFirst(e);
        }
    }

    @Override
    public void addLast(E e) {
        if (e == null) {
            throw new NullPointerException();
        } else if (tail >= elements.length && tail != FIRST_ELEMENT_INDEX) {
            increaseArraySize();
            addLast(e);
        } else if (tail < elements.length) {
            Object[] objects = new Object[elements.length + 1];
            System.arraycopy(elements, 0, objects, 0, tail);
            objects[tail] = e;
            elements = objects;
            tail++;
        } else if (tail == FIRST_ELEMENT_INDEX) {
            addFirst(e);
        }
    }

    @Override
    public E getLast() {
        E element = (E) elements[tail];
        if (element == null) {
            throw new NoSuchElementException();
        } else {
            return element;
        }
    }

    @Override
    public E getFirst() {
        E element = (E) elements[FIRST_ELEMENT_INDEX];
        if (element == null) {
            throw new NoSuchElementException();
        } else {
            return element;
        }
    }

    @Override
    public boolean offerFirst(E e) {
        addFirst(e);
        return true;
    }

    @Override
    public boolean offerLast(E e) {
        addLast(e);
        return true;
    }

    @Override
    public E peekFirst() {
        if (elements.length == 0) {
            return null;
        } else {
            return (E) elements[FIRST_ELEMENT_INDEX];
        }
    }

    @Override
    public E peekLast() {
        if (elements.length == 0) {
            return null;
        } else {
            return (E) elements[elements.length - 1];
        }
    }

    @Override
    public E pollFirst() {
        if (elements.length != 0) {
            E pollElement = (E) elements[FIRST_ELEMENT_INDEX];
            Object[] objects = new Object[elements.length - 1];
            System.arraycopy(elements, 1, objects, 0, elements.length - 1);
            elements = objects;
            return pollElement;
        } else {
            return null;
        }
    }

    @Override
    public E pollLast() {
        if (elements.length != 0) {
            E pollElement = (E) elements[tail];
            elements[tail] = null;
            return pollElement;
        } else {
            return null;
        }
    }

    @Override
    public E removeFirst() {
        E removingElement = pollFirst();
        if (removingElement == null) {
            throw new NoSuchElementException();
        } else {
            return removingElement;
        }
    }

    @Override
    public E removeLast() {
        E removingElement = pollLast();
        if (removingElement == null) {
            throw new NoSuchElementException();
        } else {
            return removingElement;
        }
    }

    public E  [] getElements() {
        Object[] objects = new Object[elements.length];
        System.arraycopy(elements, 0, objects, 0, tail);
        return (E[])objects;
    }

    public void setElements(Object[] elements) {
        this.elements = elements;
    }

    public int getTail() {
        return tail;
    }

    public void setTail(int tail) {
        this.tail = tail;
    }

    private void increaseArraySize() {
        Object[] objects = new Object[elements.length * 2];
        for (int i = 0; i < tail; i++) {
            objects[i] = elements[i];
        }
        elements = objects;
    }

    private void addToBeginng(E e) {
        Object[] tempArray = new Object[elements.length];
        System.arraycopy(elements, 0, tempArray, 1, tail);
        tempArray[FIRST_ELEMENT_INDEX] = e;
        elements = tempArray;
    }

    @Override
    public String toString() {
        return "MyDeque{" +
                "elements=" + Arrays.toString(elements) +
                ", tail=" + tail +
                ", head=" + head +
                '}';
    }

}
