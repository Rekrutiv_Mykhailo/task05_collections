package com.epam.model.deque;

public interface  DequeUtility<E> {
    void addFirst(E e);
    void addLast(E e);
    E getLast();
    E getFirst();
    boolean offerFirst(E e);
    boolean offerLast(E e);
    E peekFirst();
    E peekLast();
    E pollFirst();
    E pollLast();
    E removeFirst();
    E removeLast();

}
