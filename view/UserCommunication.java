package com.epam.view;

public interface UserCommunication {
    void output(String mess);
    int input();
}
