package com.epam.view;

import com.epam.controller.DequeController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class
MainView extends AbstractView {
    private Map<String, String> menu;
    private Map<Integer, Runner> methodsMenu;
    private Logger logger = LogManager.getLogger(AbstractView.class);
    private DequeController dequeController;

    public MainView() {
        menu = new HashMap<>();
        menu.put("1", "  1 - Deque");
        menu.put("2", "  2 - Tree");
        menu.put("0", "  0 - finish");
        methodsMenu = new HashMap<>();
        methodsMenu.put(1, this::loadFirstOptionMenu);
        dequeController = new DequeController();

    }

    public static void main(String[] args) {
        new MainView().loadMenu();
    }

    public void printMenu(Map menu) {
        for (Object o : menu.values()) {
            output(o.toString());
        }
    }

    public void loadMenu() {
        MainView mainView = new MainView();
        printMenu(mainView.getMenu());
        logger.info("Please choose option");
        mainView.methodsMenu.get(inputMenu());

    }

    public void loadFirstOptionMenu() {
        System.out.println("fdklsfj");
        menu = new HashMap<>();
        menu.put("1", "  1 - delete first element");
        menu.put("2", "  2 - delete last element");
        menu.put("3", "  3 - add first element");
        menu.put("4", "  4 - add last element");
        menu.put("5", "  5 - print deque");
        menu.put("6", "  6 - return to main menu");
        menu.put("0", "  0 - finish");
        methodsMenu = new HashMap<>();
        methodsMenu.put(1, this::outputFirstMenu);
        printMenu(menu);

    }

    public void outputFirstMenu() {
        System.out.println("dfskjslfj");
    }

    public Map<String, String> getMenu() {
        return menu;
    }

    @Override
    public void output(String mess) {
        super.output(mess);
    }

    @Override
    public Integer inputMenu() {
        return super.inputMenu();
    }

    public void printDegue() {
        logger.info(dequeController.loadAllDeque());
    }
}
